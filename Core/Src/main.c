/* USER CODE BEGIN Header */
/**
  ******************************************************************************
  * @file           : main.c
  * @brief          : Main program body
  ******************************************************************************
  * @attention
  *
  * Copyright (c) 2023 STMicroelectronics.
  * All rights reserved.
  *
  * This software is licensed under terms that can be found in the LICENSE file
  * in the root directory of this software component.
  * If no LICENSE file comes with this software, it is provided AS-IS.
  *
  ******************************************************************************
  */
/* USER CODE END Header */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

/* Private includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "stdio.h"
#include "key.h"
#include "lcd.h"
#include "gui.h"
#include "w25qxx.h"
#include "receive.h"
#include "test.h"
#include "text.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */

/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */

/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */

/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);
/* USER CODE BEGIN PFP */

/* USER CODE END PFP */

/* Private user code ---------------------------------------------------------*/
/* USER CODE BEGIN 0 */
//在USART2使用printf函数
struct __FILE
{
  int handle;
};

FILE __stdout;
//定义_sys_exit()以避免工作在半主机状态
void _sys_exit(int x) 
{ 
  x = x;
}
//重定义fputc函数
int fputc(int ch, FILE *f)
{
  uint8_t temp = (uint8_t) ch;
  HAL_UART_Transmit(&RECEIVE_USART,&temp,1,2);
  return ch;
}

/* USER CODE END 0 */

/**
  * @brief  The application entry point.
  * @retval int
  */
int main(void)
{
  /* USER CODE BEGIN 1 */
  uint8_t temp[64];
  uint8_t *gbch;
  uint32_t len = 0;
  uint32_t readlen = 0;
  uint8_t flag = 0;
//  uint8_t i = 0;
  uint16_t lastLen = 0;
  uint8_t dir = TEXT_GETTEXT_DIR_F;
  /* USER CODE END 1 */

  /* MCU Configuration--------------------------------------------------------*/

  /* Reset of all peripherals, Initializes the Flash interface and the Systick. */
  HAL_Init();

  /* USER CODE BEGIN Init */

  /* USER CODE END Init */

  /* Configure the system clock */
  SystemClock_Config();

  /* USER CODE BEGIN SysInit */

  /* USER CODE END SysInit */

  /* Initialize all configured peripherals */
  MX_GPIO_Init();
  MX_SPI1_Init();
  MX_SPI2_Init();
  MX_USART1_UART_Init();
  /* USER CODE BEGIN 2 */
  
  HAL_GPIO_WritePin(ENVCC_GPIO_Port, ENVCC_Pin, GPIO_PIN_RESET);
  HAL_GPIO_WritePin(LCD_BLK_GPIO_Port, LCD_BLK_Pin, GPIO_PIN_RESET);
  
  HAL_Delay(1000);
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_RESET);
  HAL_Delay(500);
  HAL_GPIO_WritePin(LED_GPIO_Port, LED_Pin, GPIO_PIN_SET);
  if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY2_GPIO_Port,KEY2_Pin))
  {
    HAL_GPIO_WritePin(ENVCC_GPIO_Port, ENVCC_Pin, GPIO_PIN_SET);
  }
  
  LCD_Init();
  
  
  RECEIVE_Start();

  /* USER CODE END 2 */

  /* Infinite loop */
  /* USER CODE BEGIN WHILE */
  while (1)
  {
    HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
    
    if(flag == 0)
    {
      GUI_ClearMap(0x00);

      flag = 1;
      
      if(TEXT_GETTEXT_DIR_F == dir) readlen += lastLen;
      else                          readlen -= 64;
      
      lastLen = 64;
      TEST_ReadBytes(temp, lastLen, readlen + 0x00040000);
      gbch = TEXT_GetText(temp, &lastLen, dir);
      printf("LEN: %d\r\n", lastLen);
      GUI_ShowGB2312_16x16_Moltiline(0, 0, gbch, &lastLen, Normal);
      printf("LEN2: %d\r\n", lastLen);
//      for(i = 0; i < lastLen; i++)
//      {
//        printf("%d\r\n", gbch[i]);
//      }
//      printf("\r\n");
      if(TEXT_GETTEXT_DIR_F != dir) readlen = readlen + 64 - lastLen;
      
      GUI_ShowMap();
      HAL_Delay(500);
    }
    if(KEY_UP == KEY_Scan())
    {
      dir = TEXT_GETTEXT_DIR_F;
      flag = 0;
    }
    else if(KEY_DOWN == KEY_Scan())
    {
      dir = TEXT_GETTEXT_DIR_B;
      flag = 0;
    }
    else if(KEY_LEFT == KEY_Scan())
    {
      if(readlen > 0)
      {
        readlen--;
        flag = 0;
      }
    }
    
    //if(readlen > 2498) readlen = 0;
    
    if(receive.RxState == USART_RECEIVE_STATE_DONE)
    {
      TEST_SaveBytes(receive.RxBuffer, receive.RxBufLen-1, len + 0x00040000);
      receive.RxState = USART_RECEIVE_STATE_FREE;
      len += receive.RxBufLen-1;
      printf("本次保存%d字节数据, 共已写入%d字节\r\n", receive.RxBufLen-1, len);
    }
    else if(receive.RxState == USART_RECEIVE_STATE_OVER)
    {
      printf("RECEIVE OVER!\r\n");
      receive.RxState = USART_RECEIVE_STATE_FREE;
    }
    HAL_Delay(100);
    
    /* USER CODE END WHILE */

    /* USER CODE BEGIN 3 */
  }
  /* USER CODE END 3 */
}

/**
  * @brief System Clock Configuration
  * @retval None
  */
void SystemClock_Config(void)
{
  RCC_OscInitTypeDef RCC_OscInitStruct = {0};
  RCC_ClkInitTypeDef RCC_ClkInitStruct = {0};

  /** Initializes the RCC Oscillators according to the specified parameters
  * in the RCC_OscInitTypeDef structure.
  */
  RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
  RCC_OscInitStruct.HSIState = RCC_HSI_ON;
  RCC_OscInitStruct.HSICalibrationValue = RCC_HSICALIBRATION_DEFAULT;
  RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
  RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI_DIV2;
  RCC_OscInitStruct.PLL.PLLMUL = RCC_PLL_MUL16;
  if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK)
  {
    Error_Handler();
  }

  /** Initializes the CPU, AHB and APB buses clocks
  */
  RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK|RCC_CLOCKTYPE_SYSCLK
                              |RCC_CLOCKTYPE_PCLK1|RCC_CLOCKTYPE_PCLK2;
  RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
  RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
  RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV2;
  RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV1;

  if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK)
  {
    Error_Handler();
  }
}

/* USER CODE BEGIN 4 */

/* USER CODE END 4 */

/**
  * @brief  This function is executed in case of error occurrence.
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler_Debug */
  /* User can add his own implementation to report the HAL error return state */
  __disable_irq();
  while (1)
  {
  }
  /* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t *file, uint32_t line)
{
  /* USER CODE BEGIN 6 */
  /* User can add his own implementation to report the file name and line number,
     ex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
  /* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */
