#include "lcd.h"
#include "spi.h"

void LCD_WriteCommand(uint8_t cmd)
{
  uint8_t temp = 0;
  LCD_DC_LOW();
  temp = cmd;
  HAL_SPI_Transmit(&LCDSPI, &temp, 1, 100);
}

void LCD_WriteData(uint8_t dat)
{
  uint8_t temp;
  LCD_DC_HIGH();
  temp = dat;
  HAL_SPI_Transmit(&LCDSPI, &temp, 1, 100);
}

void LCD_SetAddr(uint8_t page,uint8_t addr)
{
  LCD_WriteCommand(0xb0+ page);
  //addr += 0x04; //相较于正常情况，左右翻转时，零点存在4个像素偏差
  LCD_WriteCommand(0x10 + ((addr & 0x0f) >> 4));
  LCD_WriteCommand(addr & 0x0f);
}


void LCD_Init(void)
{
  LCD_RST_LOW();
  HAL_Delay(100);
  LCD_RST_HIGH();
  HAL_Delay(100);
  
  LCD_BLK_HIGH();
  
  LCD_CS_LOW();
  
  LCD_WriteCommand(0x2c);
  LCD_WriteCommand(0x2e);
  LCD_WriteCommand(0x2f);
  
  //升压倍数设置连续发送
  LCD_WriteCommand(0xf8); //升压倍数选择
  LCD_WriteCommand(0x00); //0x00:4倍, 0x01:5倍, 0x10:6倍
  
  LCD_WriteCommand(0x24); //选择内部电阻比例(低三位有效)
  //下两条电压指令需连续发送
  LCD_WriteCommand(0x81); //设置内部电压模式
  LCD_WriteCommand(0x20); //设置电压值(低六位有效)  //电压过高会导致屏幕颜色偏深，字迹不清晰；电压过低会导致屏幕快速刷新出现模糊
  
  LCD_WriteCommand(0xa2); //偏压比设置 0xa2:1/9, 0xa3:1/7
  LCD_WriteCommand(0xc8); //上下行扫描方向 0xc0:正常, 0xc8:反向
  LCD_WriteCommand(0xa0); //左右列扫描方向 0xa0:正常, 0xa1:反向
  LCD_WriteCommand(0xa6); //显示 0xa6:正常, 0xa7:反显
  LCD_WriteCommand(0x40); //显示起始行n(0-63) 0x40+n
  LCD_WriteCommand(0xaf); //显示 0xaf:开启, 0xae:关闭
  
  LCD_CS_HIGH();
}

void LCD_ShowBMP(uint8_t *bmp)
{
  uint8_t page = 0;
  LCD_CS_LOW();
  
  for(page = 0; page < 8; page++)
  {
    LCD_SetAddr(page,0);
    LCD_DC_HIGH();
    HAL_SPI_Transmit(&LCDSPI, bmp, 128, 100);
    bmp += 128;
  }
  
  LCD_CS_HIGH();
}
