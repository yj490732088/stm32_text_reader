#include "flashdata.h"
#include "w25qxx.h"

void FLASHDATA_Read_GB2312(uint8_t* gbch, uint8_t* temp, uint8_t size)
{
  uint16_t addr = 0;
  
  addr = 94*(*gbch - 0xa1) + *(gbch+1) - 0xa1;
  W25QXX_Read(temp, FALSHDATA_FONT_GB2312_16X16_START_ADDR + addr * size, size);
}

void FLASHDATA_Read_BNP(uint32_t addr, uint8_t* temp, uint8_t size)
{
  W25QXX_Read(temp, addr, size);
}


