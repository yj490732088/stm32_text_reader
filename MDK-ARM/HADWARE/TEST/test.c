#include "test.h"
#include "stdio.h"
#include "w25qxx.h"
#include "gui.h"


#if SAVE_FONT_TO_FLASH
  #include "GB2312_16x16_R.h"
#endif

void TEST_SearchChar(uint8_t* gbch)
{
  uint16_t addr = 0;
  uint8_t i = 0;
  uint8_t temp[32] = {0};

  addr = 94*(*gbch - 0xa1) + *(gbch+1) - 0xa1;
  printf("\r\n:Read Start %d:\r\n", addr);
  
  W25QXX_Read(temp,addr*32,32);
  for(i = 0; i < 32; i++)
  {
    printf("%d, ", temp[i]);
  }
  printf("\r\nRead End.\r\n");
}

void TEST_ShowGB2312_16x16(int x0, int y0, uint8_t* gbch, uint8_t mode)
{
  uint16_t addr = 0;
  uint8_t temp[32] = {0};

  if(*gbch >= 0xa1)
  {
    addr = 94*(*gbch - 0xa1) + *(gbch+1) - 0xa1;
    W25QXX_Read(temp,addr*32,32);
    GUI_ShowBMP(x0, y0, x0+16, y0+16, temp, mode);
  }
  else
  {
    GUI_ShowChar(x0, y0, *gbch, SIZE16, mode);
  }
}

void TEST_SaveBytes(uint8_t* temp, uint16_t size, uint32_t addr)
{
  static uint16_t index = 0;
  uint16_t i = 0;
  
  printf("保存第%d段%d个数据...\r\n", index, size);
  
  for(i = 0; i < size; i++)
  {
    printf("%d\r\n", temp[i]);
  }
  LED_ON();
  W25QXX_Write(temp,addr,size);
  LED_OFF();
  printf("第%d段数据保存完成.\r\n", index);
  HAL_Delay(100);
  HAL_GPIO_TogglePin(LED_GPIO_Port, LED_Pin);
  LED_ON();
  index++;
}

void TEST_ReadBytes(uint8_t* temp, uint16_t size, uint32_t addr)
{
//  printf("读取%d个数据...\r\n", size);
  LED_ON();
  W25QXX_Read(temp,addr,size);
  LED_OFF();
//  printf("数据读取完成.\r\n");
}

void TEST_ShowRead(uint32_t num)
{
  uint32_t addr = 0;
//  uint8_t i = 0;
  uint8_t temp[32] = {0};
  static uint8_t x0 = 0, y0 = 0;

  addr = num * 32;
  W25QXX_Read(temp,addr,32);
  GUI_ShowBMP(x0, y0, x0+16, y0+16, temp, Normal);
  
  if(x0 < 112)  x0 += 16;
  else
  {
    x0 = 0;
    if(y0 < 48) y0 += 16;
    else
    {
      y0 = 0;
    }
  }
}


#if SAVE_FONT_TO_FLASH

uint8_t Pgbch[1024] = {0};
void TEST_SaveFont(void)
{
  uint32_t addr = 0;
  uint16_t index = 0;
  uint8_t i = 0;
  uint8_t n = 0;
  
  for(index = 0; index < 256; index++)
  {
    printf("开始复制第%d段数据...\r\n", index);
    for(i = 0; i < 32; i++)
    {
      for(n = 0; n < 32; n++)
      {
        Pgbch[i*32+n] = font_gb2312_16x16[index*32+i][n];
        printf("%d\r\n", Pgbch[i*32+n]);
      }
    }
    printf("保存第%d段数据...\r\n", index);
    LED1_ON();
    W25QXX_Write(Pgbch,addr,1024);
    LED1_OFF();
    printf("第%d段数据保存完成.\r\n", index);
    addr += 1024;
    HAL_Delay(500);
    HAL_GPIO_TogglePin(LED0_GPIO_Port, LED0_Pin);
  }
  LED1_ON();
}

#endif















