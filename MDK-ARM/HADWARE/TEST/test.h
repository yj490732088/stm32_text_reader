#ifndef _TEST_H_
#define _TEST_H_


#include "main.h"

#define SAVE_FONT_TO_FLASH  0


void TEST_SearchChar(uint8_t* gbch);
void TEST_ShowGB2312_16x16(int x0, int y0, uint8_t* gbch, uint8_t mode);
void TEST_ShowRead(uint32_t num);
//void TEST_SaveFont(void);
void TEST_SaveBytes(uint8_t* temp, uint16_t size, uint32_t addr);
void TEST_ReadBytes(uint8_t* temp, uint16_t size, uint32_t addr);

#endif

