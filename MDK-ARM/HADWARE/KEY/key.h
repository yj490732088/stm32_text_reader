#ifndef _KEY_H_
#define _KEY_H_

#include "main.h"

#define KEY0  0x01
#define KEY1  0x02
#define KEY2  0x04
#define KEY3  0x08

#define KEY_UP    KEY0
#define KEY_DOWN  KEY3
#define KEY_LEFT  KEY1
#define KEY_RIGHT KEY2
#define KEY_BACK  KEY1
#define KEY_OK    KEY2

#define KEY_NUM   4
#define KEY_LONG_DOWN_TIME  10

typedef struct {
  uint8_t key_state;    //按下的按键
  uint8_t key_upping;   //按键发生抬起动作
  uint8_t key_downing;  //按键发生按下动作
  uint8_t key_keepup;   //按键保持抬起动作
  uint8_t key_keepdown; //按键保持按下动作
  uint8_t key_longdown; //按键长时间按下
} KEY_MESSAGE;      //按键信息

extern KEY_MESSAGE key_message;

void KEY_Init(void);
uint8_t KEY_Scan(void);



#endif

