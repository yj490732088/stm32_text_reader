#include "key.h"

KEY_MESSAGE key_message;
uint8_t KeyDownTime[KEY_NUM] = {0};

void KEY_Process(uint8_t key)
{
  uint8_t temp1 = 0x00;
  uint8_t temp2 = 0x00;
  uint8_t i = 0;
  
  temp1 = 0x0001;
  key_message.key_longdown = 0x0000;
  for(i = 0; i < KEY_NUM; i++)
  {
    if(key & temp1)
    {
      KeyDownTime[i]++;
      if(KeyDownTime[i] >= KEY_LONG_DOWN_TIME)
      {
        key_message.key_longdown += temp1;
        KeyDownTime[i] = KEY_LONG_DOWN_TIME;
      }
    }
    else  KeyDownTime[i] = 0;
    temp1 <<= 1;
  }
/*
  按键状态机：
  key_keepdown下按键为0(抬起状态，下同)转到key_upping，否则保持key_keepdown
  key_upping下按键为0转到key_keepup，否则转到key_downing
  key_keepup下按键为0保持key_keepup，否则转到key_downing
  key_downing下按键为0转到key_upping，否则转到key_keepdown

     __        0 --> upping-- 0        __
    /  \ 1      /    A    |  \      0 /  \
    --->keepdown<-  0|   1|   ->keepup<---
                  \  |    V    /
                 1 --downing<-- 1

*/
/*
  key表示按下的按键
  ~key表示抬起的按键
*/
  temp1 = key_message.key_keepup | key_message.key_upping;
  temp2 = key_message.key_downing | key_message.key_keepdown;
  
  key_message.key_state = key;
  key_message.key_downing = temp1 & key;
  key_message.key_keepup = temp1 & (~key);
  key_message.key_keepdown = temp2 & key;
  key_message.key_upping = temp2 & (~key);
}


uint8_t KEY_Scan(void)
{
  uint8_t key_state = 0x00;
  
  if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY0_GPIO_Port,KEY0_Pin)) key_state += KEY0;
  if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY1_GPIO_Port,KEY1_Pin)) key_state += KEY1;
  if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY2_GPIO_Port,KEY2_Pin)) key_state += KEY2;
  if(GPIO_PIN_RESET == HAL_GPIO_ReadPin(KEY3_GPIO_Port,KEY3_Pin)) key_state += KEY3;
  
  
  KEY_Process(key_state);
  
  return key_state;
}

void KEY_Init(void)
{
  key_message.key_state = 0x00;
  key_message.key_upping = 0x0f;
  key_message.key_downing = 0x00;
  key_message.key_keepup = 0x0f;
  key_message.key_keepdown = 0x00;
  key_message.key_longdown = 0x00;
}


