#include "text.h"
#include "stdio.h"

uint8_t* TEXT_GetText(uint8_t *text, uint16_t *len, uint8_t dir)
{
  uint16_t index = 0;
  uint8_t pos = 0;
  
  if(TEXT_GETTEXT_DIR_F == dir)
  {
    index = 0;
    pos = 0;
    while(1)
    {
      if(index == *len) break;  //整数个字符
      
      /*根据编码确定偏移字节数*/
      if(*(text+index) >= 0xa1) //GB2312编码
      {
        index += 2;
        pos += 2;
        if(index > *len)  //最后一个GB2312字符只有一半编码
        {
          *len -= 1;
          break;
        }
      }
      else if(*(text+index) == 0x00)
      {
        *len = index;
        break;
      }
      else //ASCII编码
      {
        if(*(text+index) == '\r')
        {
          
          if(index+1 == *len)
          {
            *len -= 1;
            break;
          }
          index += 1;
          //printf("回车r: %d, %d\r\n", index, pos);
          if(*(text+index) == '\n')
          {
            index += 1;
            pos = (pos / 16 + 1) * 16;
            //printf("回车n: %d, %d\r\n", index, pos);
            if(pos >= 64)
            {
              *len = index;
              break;
            }
          }
        }
        else
        {
          index += 1;
          pos += 1;
        }
      }
    }
    return text;
  }
  else
  {
    index = *len;
    pos = 0;
    while(1)
    {
      if(index == 0) break;  //整数个字符
      if(pos >= 64)
      {
        *len -= index+1;
        break;
      }
      
      if(*(text+index-1) >= 0xa1) //GB2312编码
      {
        if(index == 1)  //第一个GB2312字符只有一半编码
        {
          *len -= 1;
          break;
        }
        index -= 2;
        pos += 2;
      }
      else if(*(text+index) == 0x00)
      {
        *len -= index+1;
        break;
      }
      else      //ASCII编码
      {
        if(*(text+index-1) == '\n')
        {
          if(index == 1)
          {
            *len -= 1;
            break;
          }
          index -= 1;
          //printf("回车bn: %d, %d\r\n", index, pos);
          if(*(text+index-1) == '\r')
          {
            index -= 1;
            pos = (pos / 16 + 1) * 16;
            //printf("回车br: %d, %d\r\n", index, pos);
          }
        }
        else
        {
          index -= 1;
          pos += 1;
        }
        //printf("bs: %d, %d\r\n", index, pos);
      }
    }
    //printf("index: %d\r\n", index);
    return text+index;
  }
}



