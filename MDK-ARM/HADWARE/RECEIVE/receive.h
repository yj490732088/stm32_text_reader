#ifndef _RECEIVE_H_
#define _RECEIVE_H_

#include "main.h"

#define USART_RX_BUFFER_LEN     2000

#define USART_RX_START_FLAG     '#'
#define USART_RX_END_FLAG       '$'

#define USART_RECEIVE_STATE_FREE  0x00  //接收空闲
#define USART_RECEIVE_STATE_START 0x01  //接收开始
#define USART_RECEIVE_STATE_DONE  0x02  //接收完成
#define USART_RECEIVE_STATE_OVER  0x03  //接收溢出
#define USART_RECEIVE_STATE_ERROR 0x04  //接收出错

#define RECEIVE_USART  huart1

typedef struct{
  uint8_t RxData;
  uint8_t RxBuffer[USART_RX_BUFFER_LEN];
  uint16_t RxBufLen;
  uint8_t RxState;
} PARAMTER_USART;

extern PARAMTER_USART receive;

void RECEIVE_Start(void);
void RECEIVE_Decoder(void);

#endif


